# AES GCM Algorithm


## Introduction

This repository includes:

- AES cipher function.
- AES invcipher function.
- AES_GCM encryption.
- AES_GCM decryption.

The test cases are as per [this PDF](https://pdfs.semanticscholar.org/b4c4/66e7158c158fb513b729d6302521017d72fa.pdf?_ga=2.242128066.1778896172.1584287111-67902263.1584287111).

## Dependencies

- Windows 10
- gcc

## Build Instructions

1. In Windows Command Prompt:

    ```cmd
    cd <root-of-this-repository>
    make
    ```

## Testing Instructions

1. Continuing on from the Build Instructions:

    ```cmd
    bin\aes_gcm_test.exe
    ```
